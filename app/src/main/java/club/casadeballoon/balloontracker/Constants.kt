package club.casadeballoon.balloontracker

object Constants {
    val DEBUG_TAG = "CasaDeBalloon";

    val SERVICE_WAKE_LOCK = "CasaDeBalloonServiceWakeLock";
    val PHOTO_WAKE_LOCK = "CasaDeBalloonPhotoWakeLock";

    val CONFIG_PREFERENCES_KEY = "CasaDeBalloonConfig";
    val CONFIG_RUNNING_KEY = "CasaDeBalloonRunning";

    val ACTION_START_FOREGROUND_SERVICE = "ACTION_START_FOREGROUND_SERVICE";
    val ACTION_STOP_FOREGROUND_SERVICE = "ACTION_STOP_FOREGROUND_SERVICE";

    val CONFIG_FILE = "CasaDeBalloon.config.json";
    val LOG_FILE = "CasaDeBalloon.log";
    val NMEA_LOG_FILE = "CasaDeBalloon.nmea.log";
    val SENSOR_LOG_FILE = "CasaDeBalloon.sensor.log";
    val PHOTO_FILE = "CasaDeBalloon.jpg"

    val DEFAULT_EXPOSURES = listOf(0, -1);

}