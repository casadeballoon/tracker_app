package club.casadeballoon.balloontracker

import android.content.Context
import android.hardware.SensorEvent
import android.os.Build
import android.os.Environment
import android.util.Log
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*
import android.location.Location
import android.telephony.SmsManager
import java.text.DecimalFormat
import android.os.StrictMode
import java.net.HttpURLConnection
import java.net.URL
import android.hardware.Sensor
import android.content.Intent
import android.hardware.Camera.PictureCallback
import android.graphics.ImageFormat
import android.hardware.Camera
import android.net.Uri
import android.graphics.SurfaceTexture
import android.os.Debug
import kotlin.math.roundToInt


object Utils {

    val surfaceTexture = SurfaceTexture(42);

    fun getIsoString(date: Date): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US)
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(date);
    }

    fun appendToFile(file: File?, string: String?) {
        if (file != null && string != null) {
            try {
                FileOutputStream(file, true).use {
                    val writer = it.bufferedWriter();
                    writer.write(string);
                    writer.close();
                }
            } catch (e: Exception) {
                Log.e(Constants.DEBUG_TAG, "Error appending to: " + file.canonicalPath + " value: " + string);
            }
        }
    }

    fun logToFile(logFile: File?, logLine: String?) {
        appendToFile(logFile, getIsoString(Date()) + ": " + logLine + "\n");
        if (logLine != null) {
            Log.d(Constants.DEBUG_TAG, logLine);
        }
    }

    fun testStorageRoot(root: File?): Boolean {
        if (root != null) {
            Log.d(Constants.DEBUG_TAG, "Testing root " + root.canonicalPath)
            if (!root.exists() && !root.mkdirs()) {
                return false;
            }
            val file = File(root, Constants.LOG_FILE);
            try {
                FileOutputStream(file, true).use {
                    val writer = it.bufferedWriter();
                    writer.write(getIsoString(Date()));
                    writer.write(": ");
                    writer.write("Storage root validated");
                    writer.newLine();
                    writer.close();
                }
                Log.d(Constants.DEBUG_TAG, "Successfully writing to " + file.canonicalPath)
                return true;
            } catch (e: Exception) {
                Log.e(Constants.DEBUG_TAG, "Error writing to " + file.canonicalPath)
                return false;
            }
        } else {
            return false;
        }
    }

    fun getStorageRoot(context: Context): File? {
        var storageRoot: File? = null;

        // Attempt to use public external removable storage
        if (storageRoot == null) {
            val file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            if (file != null
                    && Environment.isExternalStorageRemovable()
                    && Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)
                    && testStorageRoot(file)) {
                storageRoot = file;
                Log.d(Constants.DEBUG_TAG, "Public external removable storage found at: " + file.canonicalPath);
            }
        }

        // Attempt to use Lollipop+ private external removable storage
        if (storageRoot == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val files = context.applicationContext.getExternalFilesDirs(Environment.DIRECTORY_PICTURES);
            files.forEach { file ->
                if (file != null
                        && Environment.isExternalStorageRemovable(file)
                        && Environment.getExternalStorageState(file).equals(Environment.MEDIA_MOUNTED)
                        && testStorageRoot(file)) {
                    storageRoot = file;
                    Log.d(Constants.DEBUG_TAG, "Lollipop+ private external removable storage found at: " + file.canonicalPath);
                }
            }
        }

        // Attempt to use secondary storage
        if (storageRoot == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val secondaryStorage = System.getenv("SECONDARY_STORAGE") ?: "";
            val externalSdStorage = System.getenv("EXTERNAL_SDCARD_STORAGE") ?: "";
            val sdStorage = if (secondaryStorage.length > 0) secondaryStorage else externalSdStorage;
            val files = context.applicationContext.getExternalFilesDirs(Environment.DIRECTORY_PICTURES);
            files.forEach { file ->
                if (file != null
                        && sdStorage.length > 0
                        && file.canonicalPath.startsWith(sdStorage)
                        && testStorageRoot(file)) {
                    storageRoot = file;
                    Log.d(Constants.DEBUG_TAG, "Secondary storage found at: " + file.canonicalPath);
                }
            }
        }

        // Attempt to use any storage
        if (storageRoot == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val files = context.applicationContext.getExternalFilesDirs(Environment.DIRECTORY_PICTURES);
            files.forEach { file ->
                if (file != null
                        && testStorageRoot(file)) {
                    storageRoot = file;
                    Log.d(Constants.DEBUG_TAG, "Private external storage found at: " + file.canonicalPath);
                }
            }
        }

        // Attempt to check for Samsung /extSdCard
        if (storageRoot == null && Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            if(File("/storage/extSdCard").exists()) {
                var file = File("/storage/extSdCard/CasaDeBalloon");
                file.mkdirs();
                if(file.exists() && testStorageRoot(file)) {
                    storageRoot = file;
                    Log.d(Constants.DEBUG_TAG, "Samsung legacy SD storage found at: " + file.canonicalPath);
                }
            }
        }

        // Attempt to use any storage (legacy)
        if (storageRoot == null) {
            val file = Environment.getExternalStorageDirectory()
            if (file != null
                    && Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)
                    && testStorageRoot(file)) {
                storageRoot = file;
                Log.d(Constants.DEBUG_TAG, "Private legacy external storage found at: " + file.canonicalPath);
            }
        }

        return storageRoot;
    }


    fun getLocationString(location: Location?): String {

        val timestampFormat = SimpleDateFormat("HH:mm:ss");
        val timestamp = if (location?.time != null) {
            timestampFormat.format(Date(location.time))
        } else {
            "??:??:??"
        }

        val posFormat = DecimalFormat("#.########")
        val latitude = if (location?.latitude != null) {
            posFormat.format(location.latitude)
        } else {
            "???"
        }
        val longitude = if (location?.longitude != null) {
            posFormat.format(location.longitude)
        } else {
            "???"
        }

        val altaccFormat = DecimalFormat("#.##")
        val altitude = if (location?.altitude != null) {
            altaccFormat.format(location.altitude)
        } else {
            "??"
        }
        val accuracy = if (location?.accuracy != null) {
            altaccFormat.format(location.accuracy)
        } else {
            "??"
        }

        return (timestamp + " http://maps.google.com/?q=" + latitude + ","
                + longitude + " Alt:" + altitude + "m Acc:" + accuracy + "m")
    }

    fun sendLocationSMS(location: Location?) {
        val smsManager = SmsManager.getDefault();
        val location = Utils.getLocationString(location);
        val sms = Config.name + ": " + location;
        Log.d(Constants.DEBUG_TAG, "Attempting to send SMS to " + Config.smsPhoneNumber + ": " + sms);
        smsManager.sendTextMessage(Config.smsPhoneNumber, null, sms, null, null);
    }

    fun sendTrackerPing(location: Location?) {
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US)
        dateFormat.timeZone = TimeZone.getTimeZone("UTC")

        val ts = if (location?.time == null) "1900-01-01T00:00:00Z" else dateFormat.format(Date(location.time))
        val lat = if (location?.latitude == null) "0.1" else location.latitude.toString()
        val lon = if (location?.longitude == null) "0.2" else location.longitude.toString()
        val alt = if (location?.altitude == null) "0.3" else location.altitude.toString()

        val dataUrl = (Config.trackerBaseUrl + "&device=" + Config.name
                + "&ts=" + ts
                + "&lat=" + lat
                + "&lng=" + lon
                + "&alt=" + alt
                + "&notes=")

        Log.d(Constants.DEBUG_TAG, "Attempting to ping tracker: " + dataUrl);

        val urlConnection = URL(dataUrl).openConnection() as HttpURLConnection
        urlConnection.connectTimeout = 10000;
        urlConnection.readTimeout = 10000;
        urlConnection.getResponseCode();
        urlConnection.disconnect();
    }

    fun logSensorData(sensorLogFile: File?, sensorEvents: List<SensorEvent>?, location: Location?, batteryMillivolts: Int?) {

        val sensorSummary = StringBuilder();

        val logTime = getIsoString(Date());

        // GPS
        try {
            if(location?.latitude != null
                    && location?.longitude != null
                    && location?.altitude != null
                    && location?.time != null) {
                val sensorTime = getIsoString(Date(location.time));
                val sensorLine = logTime + "," + sensorTime + "," +
                        "GPS" + "," +
                        "lat" + "," + location.latitude.toString() + "," +
                        "lon" + "," + location.longitude.toString() + "," +
                        "alt_m" + "," + location.altitude.toString()
                sensorSummary.appendln(sensorLine);
            }
        } catch (e: Exception) {
            Log.e(Constants.DEBUG_TAG, "Error getting GPS sensor line", e)
        }


        // Battery
        try {
            if(batteryMillivolts != null) {
                val sensorLine = logTime + "," + logTime + "," +
                        "Battery" + "," +
                        "mV" + "," + batteryMillivolts.toString()
                sensorSummary.appendln(sensorLine);
            }
        } catch (e: Exception) {
            Log.e(Constants.DEBUG_TAG, "Error getting Battery sensor line", e)
        }

        // Sensor events
        if(sensorEvents != null) {
            for (sensorEvent in sensorEvents) {
                try {
                    val sensorTime = getIsoString(Date(sensorEvent.timestamp));
                    var unit = "raw"
                    if (sensorEvent.sensor.getType() == Sensor.TYPE_PRESSURE) {
                        unit = "hPa"
                    }
                    if (sensorEvent.sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE) {
                        unit = "degC"
                    }
                    if (sensorEvent.sensor.getType() == Sensor.TYPE_RELATIVE_HUMIDITY) {
                        unit = "pct"
                    }
                    val sensorLine = logTime + "," + sensorTime + "," +
                            sensorEvent.sensor.name + "," +
                            unit + "," + sensorEvent.values[0].toDouble().toString()
                    sensorSummary.appendln(sensorLine);
                } catch (e: Exception) {
                    Log.e(Constants.DEBUG_TAG, "Error getting sensor line", e)
                }

            }
        }

        appendToFile(sensorLogFile, sensorSummary.toString());

        Log.d(Constants.DEBUG_TAG, "Logged sensors to "+(sensorLogFile?.canonicalPath ?: "/unknown"));
    }

    fun reboot() {
        val proc = Runtime.getRuntime().exec(
                arrayOf("su", "-c", "reboot"))
        proc.waitFor()
    }


    fun takePhotoBurst(context: Context, location: Location?, exposures: List<Pair<File,Int>>, onComplete: (Boolean) -> Unit) {
        if(exposures.size == 0) {
            onComplete(true);
        } else {
            val photoFile = exposures[0].first;
            val exposure = exposures[0].second;
            takePhoto(context, photoFile, location, exposure, {isSuccess ->
                if(isSuccess) {
                    val remainingExposures = exposures.subList(1, exposures.size);
                    takePhotoBurst(context, location, remainingExposures, onComplete);
                } else {
                    onComplete(false);
                }
            });
        }
    }

    fun takePhoto(context: Context, photoFile: File, location: Location?, exposure: Int, onComplete: (Boolean) -> Unit) {
        setCameraParams(location, exposure);
        try {
            Status.camera?.setPreviewTexture(surfaceTexture)
            Status.camera?.startPreview();
            Thread.sleep(500);
            Status.camera?.takePicture(null, null, object : PictureCallback {
                override fun onPictureTaken(data: ByteArray, camera: Camera) {
                    Log.d(Constants.DEBUG_TAG, "PictureCallback onPictureTaken")
                    Thread.sleep(500);
                    var result = true;
                    try {
                        val fos = FileOutputStream(photoFile)
                        fos.write(data)
                        fos.close()
                        context.sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(photoFile)))
                        Log.d(Constants.DEBUG_TAG, "Saved photo: "+photoFile.canonicalPath)
                    } catch (e: Exception) {
                        Log.e(Constants.DEBUG_TAG, "Error saving photo to file: "+photoFile.canonicalPath, e);
                        result = false;
                    }
                    try {
                        Status.camera!!.stopPreview();
                        Thread.sleep(500);
                    } catch (e: Exception) {
                        Log.e(Constants.DEBUG_TAG, "Error stopping camera preview", e);
                        result = false;
                    }
                    onComplete.invoke(result);
                }
            })
        } catch (e: Exception) {
            Log.e("CasaDeBalloon", "Error previewing and taking picture", e)
            onComplete.invoke(false);
        }
    }

    fun openCamera() {
        Log.d(Constants.DEBUG_TAG, "Opening camera");
        if(Status.camera == null) {
            Log.d(Constants.DEBUG_TAG, "Actually opening camera");
            for(i in 3 downTo 0) {
                try {
                    Status.camera = Camera.open();
                    break;
                } catch (e: Exception) {
                    Log.e(Constants.DEBUG_TAG, "Error opening camera", e);
                    Thread.sleep(500);
                }
                if(Status.camera == null && i == 0) {
                    reboot();
                }
            }
            Thread.sleep(500);
        }
    }

    fun setCameraParams(location: Location?, exposure: Int) {
        var params = Status.camera!!.getParameters()

        val previewSizes = params.getSupportedPreviewSizes();
        params.setPreviewSize(previewSizes!!.get(0)!!.width, previewSizes!!.get(0)!!.height);

        setPreferredValue(params::setFlashMode, params.supportedFlashModes, listOf(
                Camera.Parameters.FLASH_MODE_OFF))

        setPreferredValue(params::setFocusMode, params.supportedFocusModes, listOf(
                Camera.Parameters.FOCUS_MODE_INFINITY,
                Camera.Parameters.FLASH_MODE_AUTO,
                Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))

        setPreferredValue(params::setSceneMode, params.supportedSceneModes, listOf(
                Camera.Parameters.SCENE_MODE_ACTION,
                Camera.Parameters.SCENE_MODE_SPORTS))

        params.setJpegQuality(100)

        params.setPictureFormat(ImageFormat.JPEG)

        val exposureStep = (exposure.toFloat() / params.exposureCompensationStep).roundToInt()
        params.setExposureCompensation(Math.min(Math.max(exposureStep, params.minExposureCompensation), params.maxExposureCompensation));

        setPreferredValue(params::setWhiteBalance, params.supportedWhiteBalance, listOf(
                Camera.Parameters.WHITE_BALANCE_AUTO))
        setPreferredValue(params::setColorEffect, params.supportedColorEffects, listOf(
                Camera.Parameters.EFFECT_NONE))

        params.setAutoExposureLock(false)
        params.setAutoWhiteBalanceLock(false)
        var maxSize = params.getPictureSize()
        for (size in params.getSupportedPictureSizes()) {
            if (size.width * size.height > maxSize.width * maxSize.height) {
                maxSize = size
            }
        }
        params.setPictureSize(maxSize.width, maxSize.height)

        if(location?.latitude != null) {
            params.setGpsLatitude(location.latitude)
        }
        if(location?.longitude != null) {
            params.setGpsLongitude(location.longitude)
        }
        if(location?.altitude != null) {
            params.setGpsAltitude(location.altitude)
        }
        if(location?.time != null) {
            params.setGpsTimestamp(location.time)
        }

        try {
            Status.camera!!.setParameters(params)
        } catch (ex: Throwable) {
            Log.e(Constants.DEBUG_TAG, "Error setting initial camera parameters", ex)
        }
    }

    fun closeCamera() {
        Log.d(Constants.DEBUG_TAG, "Closing camera");
        if(Status.camera != null) {
            try {
                Log.d(Constants.DEBUG_TAG, "Actually closing camera");
                Status.camera?.release();
            } catch (e: Exception) {
                Log.e(Constants.DEBUG_TAG, "Error releasing camera", e);
            }
            Status.camera = null;
        }
    }

    fun setPreferredValue(setter: (String) -> Unit, options: Collection<String>?, preferences: Collection<String>) {
        var choice: String? = null;
        for(preference in preferences) {
            if(options?.contains(preference) ?: false) {
                choice = preference;
                break;
            }
        }
        if(choice != null) {
            setter.invoke(choice);
        }
    }
}