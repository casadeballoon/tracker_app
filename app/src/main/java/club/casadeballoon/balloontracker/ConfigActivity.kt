package club.casadeballoon.balloontracker

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import java.io.File
import java.io.BufferedReader
import java.io.FileOutputStream
import java.io.FileReader


class ConfigActivity : AppCompatActivity() {

    var configFile: File? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
    }

    override fun onResume() {
        super.onResume();
        configFile = File(Utils.getStorageRoot(this), Constants.CONFIG_FILE);
        refresh();
    }

    fun refresh() {
        val configPathText = findViewById<TextView>(R.id.configPathText);
        configPathText.text = configFile?.canonicalPath ?: "/unknown";

        Config.readFromPreferences(this);
        val configJsonEdit = findViewById<TextView>(R.id.configJsonEdit);
        configJsonEdit.text = Config.serializeToString();
    }


    fun onDoneClick(v: View) {
        finish();
    }

    fun onSaveClick(v: View) {
        val configJsonEdit = findViewById<TextView>(R.id.configJsonEdit);
        val json = configJsonEdit.text.toString().trim();
        if(json.startsWith("{") && json.endsWith("}")) {
            Config.readFromString(json);
        }
        Config.saveToPreferences(this);
        if(configFile != null) {
            val json = Config.serializeToString();
            try {
                FileOutputStream(configFile, false).use {
                    val writer = it.bufferedWriter();
                    writer.write(json);
                    writer.newLine();
                    writer.close();
                }
                Log.d(Constants.DEBUG_TAG, "Config saved to file: "+configFile!!.canonicalPath);
            } catch (e: Exception) {
                Log.e(Constants.DEBUG_TAG, "Error writing to: "+(configFile?.canonicalPath ?: "/unknown") + " value: "+json);
            }
        }
        refresh();
    }

    fun onLoadClick(v: View) {
        if(configFile?.exists() ?: false) {
            val text = StringBuilder()
            BufferedReader(FileReader(configFile)).use { reader ->
                var line: String? = reader.readLine();
                while(line != null) {
                    text.append(line);
                    text.append('\n');
                    line = reader.readLine();
                }
            }
            val json = text.toString().trim();
            if(json.startsWith("{") && json.endsWith("}")) {
                Config.readFromString(text.toString());
                Log.d(Constants.DEBUG_TAG, "Config loaded from file: "+configFile!!.canonicalPath);
            }
        }
        Config.saveToPreferences(this);
        refresh();
    }

}
